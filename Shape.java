import java.awt.Point;
import java.awt.geom.GeneralPath;

class Shape {

    public java.awt.Shape shape;
    public java.awt.BasicStroke stroke;
    public java.awt.Paint paint;
    public int width = 25;
    public int height = 25;

    private static int DEFAULT_SIZE_OF_SHAPE = 25;

    /**
    *   Consctructor of needed shape
    *
    *   @param type Represents type of shapes to draw. Two digit number
    *   @param property Represents additional properrty of drawed shape
    *
    *   @see ShapeType
    *   @see ShapeProperty
    *
    *   @throws Error in case of using not supported number for shape or property
    */
    public Shape(ShapeType type, ShapeProperty property) {
        this(type, property, DEFAULT_SIZE_OF_SHAPE, DEFAULT_SIZE_OF_SHAPE);
    }

    public Shape(ShapeType type, ShapeProperty property, int width, int height) {
        this.width = width;
        this.height = height;

        this.stroke = new java.awt.BasicStroke(3.0F);

        switch (type) {
            case HEXAHEDRON:
                this.shape = createStar(3, new Point(0, 0), this.width / 2.0D, this.width / 2.0D);
                break;
            case STAR_5_ANGLED:
                this.shape = createStar(5, new Point(0, 0), this.width / 2.0D, this.width / 4.0D);
                break;
            case SQUARE:
                this.shape = new java.awt.geom.Rectangle2D.Double(-this.width / 2.0D, -this.height / 2.0D, this.width, this.height);
                break;
            case TRIANGLE:
                GeneralPath path = new GeneralPath();
                double tmp_height = Math.sqrt(2.0D) / 2.0D * this.height;
                path.moveTo(-this.width / 2, -tmp_height);
                path.lineTo(0.0D, -tmp_height);
                path.lineTo(this.width / 2, tmp_height);
                path.closePath();
                this.shape = path;
                break;
            case SEGMENT:
                this.shape = new java.awt.geom.Arc2D.Double(-this.width / 2.0D, -this.height / 2.0D, this.width, this.height, 30.0D, 300.0D, 2);
                break;
            default:
                throw new Error("Type not yet supported");
        }

        switch (property) {
            case DEFAULT:
            case LINE_WIDTH_3PX:
                this.stroke = new java.awt.BasicStroke(3.0F);
                break;
            case LINE_WIDTH_7PX:
                this.stroke = new java.awt.BasicStroke(7.0F);
                break;
            case COLOR_GRAY_GRADIENT:
                this.paint = new java.awt.GradientPaint(
                    -this.width, -this.height, java.awt.Color.white,
                    this.width, this.height, java.awt.Color.gray, true);
                break;
            case COLOR_RED:
                this.paint = java.awt.Color.red;
                break;
            default:
                throw new Error("Property not yet supported");
        }
    }

    /**
    *   Static method to create star Shape
    *
    *   @param arms Count of angles for star
    *   @param center Cootdinates of center point of shape
    *   @param rOuter Radius of outer part of star
    *   @param rInner Radius of inner part of star
    */
    private static java.awt.Shape createStar(int arms, Point center, double rOuter, double rInner) {
        double angle = 3.141592653589793D / arms;

        GeneralPath path = new GeneralPath();

        for (int i = 0; i < 2 * arms; i++){
            double r = (i & 0x1) == 0 ? rOuter : rInner;
            java.awt.geom.Point2D.Double p = new java.awt.geom.Point2D.Double(center.x + Math.cos(i * angle) * r, center.y + Math.sin(i * angle) * r);
            if (i == 0) {
                path.moveTo(p.getX(), p.getY());
            } else {
                path.lineTo(p.getX(), p.getY());
            }
        }

        path.closePath();
        return path;
    }
}