import javax.swing.JFrame;

public class TitlesFrame extends JFrame {
    /**
    *   Constructor of main window
    */
    public TitlesFrame() {
        initUI();
    }

    /**
    *   Initialization of main window
    */
    private void initUI() {
        setTitle("Кривые фигуры");
        setDefaultCloseOperation(3);
        add(new TitlesPanel(ShapeType.STAR_5_ANGLED, ShapeProperty.LINE_WIDTH_3PX));
        setSize(350, 350);
        setLocationRelativeTo(null);
    }

    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                TitlesFrame ps = new TitlesFrame();
                ps.setVisible(true);
            }
        });
    }
}
