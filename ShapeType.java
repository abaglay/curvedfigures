public enum ShapeType {
    HEXAHEDRON,
    STAR_5_ANGLED,
    SQUARE,
    TRIANGLE,
    SEGMENT
}