public enum ShapeProperty {
    DEFAULT,
    LINE_WIDTH_3PX,
    LINE_WIDTH_7PX,
    COLOR_GRAY_GRADIENT,
    COLOR_RED
}