public class ShapeFactory {

    /**
    *   Consctructor of shape factory
    */
    private ShapeFactory() {
    }

    /**
    *   Method creates Shape object
    *
    *   @param type Represents type of shapes to draw. Two digit number
    *   @param property Represents additional properrty of drawed shape
    *
    *   @see ShapeType
    *   @see ShapeProperty
    *
    *   @throws Error in case of using not supported number for shape or property
    */
    public static Shape createShapeWithProperty(ShapeType type, ShapeProperty property) {
        return new Shape(type, property);
    }

    public static Shape createShapeWithProperty(ShapeType type, ShapeProperty property, int width, int height) {
        return new Shape(type, property, width, height);
    }

}
